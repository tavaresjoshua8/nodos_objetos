import java.util.Scanner;

import util.Solicitante;
import util.nodos.ColaSolicitante;

public class App {
    private final static Scanner input = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        ColaSolicitante cola = new ColaSolicitante();
        int opcion = 0;
        do {
            opcion = menu();
            input.nextLine();
            switch (opcion) {
                case 1:
                    System.out.println("\n****INGRESE DATOS DE LA PERSONA****");
                    System.out.print("Nombre: \n> ");
                    String nombre = input.nextLine();
                    System.out.print("Apellido: \n> ");
                    String apellido = input.nextLine();
                    System.out.print("Edad: \n> ");
                    int edad = input.nextInt();
                    input.nextLine();
                    System.out.print("¿Tiene identificación? (S/N)\n> ");
                    String identificacion = input.nextLine();
                    boolean identificacionBool = (identificacion.equalsIgnoreCase("S")) ? true : false;
                    System.out.print("¿Tiene USB? (S/N)\n> ");
                    String usb = input.nextLine();
                    boolean usbBool = (usb.equalsIgnoreCase("S")) ? true : false;
                    Solicitante solicitante = new Solicitante(nombre, apellido, edad, identificacionBool, usbBool);
                    cola.offer(solicitante);
                    break;
                case 2:
                    if (cola.isEmpty()) {
                        System.out.println("La cola esta vacía.");
                        break;
                    }
                    System.out.println("Persona atendida: \n" + cola.poll());
                    break;
                case 3:
                    if(cola.isEmpty()) {
                        System.out.println("La cola esta vacía.");
                        break;
                    }
                    System.out.println(cola.peek());
                    break;
                case 4:
                    System.out.println("La cola esta vacía: " + cola.isEmpty());
                    break;
                case 5:
                    System.out.println("La cola tiene " + cola.size() + " elementos.");
                    break;
                case 0:
                    System.out.println("Finalizando Programa...");
                    break;
                default:
                    System.out.println("Opción inválida.");
                }
        } while (opcion != 0);
    }

    public static int menu() {
        System.out.println("\nOPCIONES DEL PROGRAMA");
        System.out.println("1.- Formar persona en la Cola.");
        System.out.println("2.- Atender persona de la Cola.");
        System.out.println("3.- Persona que esta al inicio de la Cola.");
        System.out.println("4.- ¿La cola esta vacia?");
        System.out.println("5.- ¿Cuántas personas hay en la cola?");
        System.out.println("0.- Salir.");
        System.out.print("Opcion: \n> ");

        return input.nextInt();
    }
}
