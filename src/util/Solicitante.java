package util;

public class Solicitante {
    private String nombre;
    private String apellido;
    private int edad;
    private boolean identificacion;
    private boolean usb;

    public Solicitante(String nombre, String apellido, int edad, boolean identificacion, boolean usb) {
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setEdad(edad);
        this.setIdentificacion(identificacion);
        this.setUsb(usb);
    }

    // Getters
    public String getNombre() { return this.nombre; }
    public String getApellido() { return this.apellido; }
    public int getEdad() { return this.edad; }
    public boolean hasIdentificacion() { return this.identificacion; }
    public boolean hasUsb() { return this.usb; }

    // Setters
    public void setNombre(String nombre) { this.nombre = nombre; }
    public void setApellido(String apellido) { this.apellido = apellido; }
    public void setEdad(int edad) { this.edad = edad; }
    public void setIdentificacion(boolean identificacion) { this.identificacion = identificacion; }
    public void setUsb(boolean usb) { this.usb = usb; }

    @Override
    public String toString() {
        return "Solicitante \n" +
                this.nombre + ' ' + this.apellido + "\n" +
                "Edad: " + edad + "\n"
                + "Identificación: " + (this.identificacion ? "Si" : "No") + "\n"
                + "USB: " + (this.usb ? "Si" : "No");
    }
}