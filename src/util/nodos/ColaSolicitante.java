package util.nodos;

import util.Solicitante;

public class ColaSolicitante {
    NodoSolicitante inicio = null;
    NodoSolicitante fin = null;
    int tamanio = 0;

    public ColaSolicitante() { }

    public void offer(Solicitante solicitante) {
        NodoSolicitante nuevo = new NodoSolicitante(solicitante);
        if(isEmpty()) {
            inicio = nuevo;
        } else {
            fin.siguiente = nuevo;
        }
        fin = nuevo;
        tamanio++;
    }

    // Elimina el primer elemento de la cola
    public Solicitante poll() {
        if(isEmpty()) { return null; }

        Solicitante temp = inicio.solicitante;
        inicio = inicio.siguiente;
        tamanio--;
        return temp;
    }

    // Devuelve el primer elemento de la cola
    public Solicitante peek() {
        if(isEmpty()) { return null; }
        return inicio.solicitante;
    }

    public int size() { return tamanio; }
    public boolean isEmpty() { return inicio == null; }
}
