package util.nodos;

import util.Solicitante;

public class NodoSolicitante {
    Solicitante solicitante;
    NodoSolicitante siguiente;

    public NodoSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
        this.siguiente = null;
    }
}
